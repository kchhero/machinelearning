import pickle
import numpy as np
import time
from PIL import Image

def imgshow(img):
    print(str(Image.fromarray(img)))
    return Image.fromarray(img)

def getTime():
    return time.time()

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def softmax(a):
    c = np.max(a)
    exp_a = np.exp(a-c)
    sum_exp_a = np.sum(exp_a)
    y = exp_a / sum_exp_a

    return y

def mean_squared_error(y,t):
    return 0.5*np.sum((y-t)**2)
                                        
