import numpy as np
import mylibrary
import matplotlib.pylab as plt

def test1():
    x = np.array([[0.9, 0.8, 0.1],[0.1, 0.8, 0.1], [0.3, 0.1, 0.6], [0.2, 0.5, 0.3],[0.2, 0.1, 0.3], [0.8, 0.1, 0.1]])
    print(x)
    p = np.argmax(x, axis=0)
    t = np.argmax(x, axis=1)
    print(p)
    print(t)

def test2():
    x = np.arange(0.0, 20.0, 0.1)
    y = 0.01*x**2 + 0.1*x
    plt.xlabel("x")
    plt.ylabel("f(x)")
    plt.plot(x,y)
    plt.show()

def function_2(x):
    return x[0]**2 + x[1]**2

def test3():
    x = np.array([[1,2,3],[1,2,3],[1,2,3]])
    print(np.sum(x, axis=0))
    print(np.sum(x, axis=1))


def function_23(x):
    return x[0]**2 + x[1]**2

mylib = mylibrary.mylibs()

#print(mylib.numerical_gradient(function_2, np.array([3.0, 4.0])))

init_x = np.array([-3.0, 4.0])
print(mylib.gradient_descent(function_23, init_x=init_x, lr=0.1, step_num=100))
