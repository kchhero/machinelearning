
# coding: utf-8

# In[16]:

import nbimporter
import mnist as mn
import pickle
import numpy as np
import time

def get_data():
    (x_train, t_train), (x_test, t_test) = mnmn.load_mnist(normalize=True, flatten=True, one_hot_label=False)
    return x_test, t_test

def get_data_mini():
    (x_train, t_train), (x_test, t_test) = mnmn.load_mnist(normalize=True, one_hot_label=True)
    return x_test, t_test, x_train, t_train

def init_network():
    with open("sample_weight.pkl", 'rb') as f:
        network = pickle.load(f)

    return network

def predict(network, x):
    W1, W2, W3 = network['W1'], network['W2'], network['W3']
    b1, b2, b3 = network['b1'], network['b2'], network['b3']

    a1 = np.dot(x, W1) + b1
    z1 = sigmoid(a1)
    
    a2 = np.dot(z1, W2) + b2
    z2 = sigmoid(a2)
    
    a3 = np.dot(z2, W3) + b3
    y = softmax(a3)

    return y

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def softmax(a):
    c = np.max(a)
    exp_a = np.exp(a-c)
    sum_exp_a = np.sum(exp_a)
    y = exp_a / sum_exp_a

    return y

def batch_test1():
    ttt = time.time()
    print("START time:"+str(ttt))
    x, t = get_data()
    network = init_network()
    batch_size = 100

    accuracy_cnt = 0
    for i in range(0, len(x), batch_size):
        x_batch = x[i:i+batch_size]
        y_batch = predict(network, x_batch)
        p = np.argmax(y_batch, axis=1)
        print(y_batch.shape)
        print(y_batch)
        break
        accuracy_cnt += np.sum(p==t[i:i+batch_size])

    print("Accuracy:" + (str(float(accuracy_cnt) / len(x))))
    print("START time:"+str(time.time()-ttt))
    
def batch_test2():
    xtest,ttest,xtrain, ttrain = get_data_mini()
    print(xtrain.shape)
    print(ttrain.shape)
    
    train_size = xtrain.shape[0]
    print(train_size)
    
    batch_size = 10
    batch_mask = np.random.choice(train_size, batch_size)
    print(batch_mask)
    
    x_batch = xtrain[batch_mask]
    t_batch = ttrain[batch_mask]
    print("x_batch\n",x_batch)
    print("x_batch-type\n",type(x_batch))
    print(t_batch)
    
mnmn = mn.mnistcls()
#batch_test1
batch_test2()


# asdf

# In[ ]:



