# coding: utf-8
import mnist as mn
import sys, os
sys.path.append(os.pardir)  # 부모 디렉터리의 파일을 가져올 수 있도록 설정
import numpy as np
from PIL import Image
import mylibrary

pil_img=None

def img_show(img):
    global pil_img
    pil_img = Image.fromarray(img)
    pil_img.show()
    print(pil_img)

mnmn = mn.mnistcls()
(x_train, t_train), (x_test, t_test) = mnmn.load_mnist(flatten=True, normalize=False)

img = x_train[0]
label = t_train[0]
print(label)  # 5
#print(img)
print(len(img))
print(img.shape)  # (784,)
img = img.reshape(28, 28)  # 형상을 원래 이미지의 크기로 변형
print(img.shape)  # (28, 28)

mylib = mylibrary.mylibs()

print(mylib.getTime())
imgInstance = mylib.imgshow(img)
print("imgInstance : "+str(imgInstance))
imgInstance.show()


