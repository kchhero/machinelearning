import numpy as np
import mylibrary
import matplotlib.pylab as plt

class simpleNet:
    mylib = mylibrary.mylibs()
    
    def __init__(self):
        self.W = np.random.randn(2,3)
        print("net.W = \n"+str(self.W)+"\n")

    def predict(self, x):
        temp = np.dot(x, self.W)
        print("predict = "+str(temp)+"\n")
        return temp

    def loss(self,x,t):
        z = self.predict(x)
        y = self.mylib.softmax(z)
        loss = self.mylib.cross_entropy_error(y, t)

        print("loss = "+str(loss)+"\n")
        return loss

print("dummy")

net = simpleNet()
x = np.array([0.6, 0.9])
print("x = "+str(x))

t=np.array([0,0,1])
net.loss(x,t)
