# coding: utf-8
import mnist as mn
import pickle
import numpy as np
import time
import twolayernet as tln

mnmn = mn.mnistcls()

(x_train, t_train), (x_test, t_test) = mnmn.load_mnist(normalize=True, one_hot_label=True)

train_loss_list = []

iters_num = 10
train_size = x_train.shape[0]
print("train_size = " + str(train_size))
batch_size = 100
learning_rate = 0.1
network = tln.TwoLayerNet(input_size=784, hidden_size=50, output_size=10)

for i in range(iters_num):
    batch_mask = np.random.choice(train_size, batch_size)
    print("batch_mask = "+str(batch_mask))
    
    x_batch = x_train[batch_mask]
    t_batch = t_train[batch_mask]
    print("x_batch = " + str(x_batch))
    print("t_batch = " + str(t_batch))
    
    grad = network.numerical_gradient(x_batch, t_batch)
    print("grad = " + str(grad))
    
    for key in ('W1', 'b1', 'W2', 'b2'):
        print("key = " + str(key))
        network.params[key] -= learning_rate * grad[key]
        print(learning_rate * grad[key])

    loss = network.loss(x_batch,  t_batch)
    print("loss = " + str(loss))
    train_loss_list.append(loss)
    
