import pickle
import numpy as np
import time
from PIL import Image

class mylibs:
    def imgshow(self,img):
        print("mylib imgshow run")
        return Image.fromarray(img)

    def getTime(self):
        return time.time()

    def sigmoid(self,x):
        return 1 / (1 + np.exp(-x))

    def softmax(self,a):
        c = np.max(a)
        exp_a = np.exp(a-c)
        sum_exp_a = np.sum(exp_a)
        y = exp_a / sum_exp_a

        return y

    def mean_squared_error(self,y,t):
        return 0.5*np.sum((y-t)**2)

    def cross_entropy_error(self,y,t):
        if y.ndim == 1:
            t = t.reshape(1, t.size)
            y = y.reshape(1, y.size)
        

        batch_size = y.shape[0]
        #return -np.sum(np.log(y[np.arange(batch_size),t])) / batch_size   #==> ont_hot_lable=FALSE
        return -np.sum(t*np.log(y)) / batch_size

    def numerical_diff(self,f,x):
        h = 1e-4
        return (f(x + h) - f(x-h)) / (2*h)

    def numerical_gradient(self,f,x):
        h = 1e-4
        grad = np.zeros_like(x)
        print("grad " + str(grad))
        print("x size : " + str(x.size))
        for idx in range(x.size):
            tmp_val = x[idx]
            x[idx] = tmp_val + h
            fxh1 = f(x)

            x[idx] = tmp_val -h
            fxh2 = f(x)

            grad[idx] = ((fxh1 - fxh2) / (2*h))
            x[idx] = tmp_val

        return grad

    def gradient_descent(self, f, init_x, lr=0.01, step_num=100):
        x = init_x
        for i in range(step_num):
            grad = self.numerical_gradient(f, x)
            x = x - lr * grad
            print("grad =  "+str(grad))
            print("%d x =  "%i+str(x))
        return x
