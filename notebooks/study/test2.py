import numpy as np
import matplotlib.pylab as plt
import pickle
import time
from PIL import Image

def numerical_gradient(f,x):
    h = 1e-4
    grad = np.zeros_like(x)
    print("grad " + str(grad))
    print("x size : " + str(x.size))
    for idx in range(x.size):
        tmp_val = x[idx]
        x[idx] = tmp_val + h
        fxh1 = f(x)

        x[idx] = tmp_val -h
        fxh2 = f(x)

        grad[idx] = ((fxh1 - fxh2) / (2*h))
        x[idx] = tmp_val

    return grad

def gradient_descent(f, init_x, lr=0.01, step_num=100):
    x = init_x
    for i in range(step_num):
        grad = numerical_gradient(f, x)
        x = x - lr * grad
        print("grad =  "+str(grad))
        print("%d x =  "%i+str(x))
    return x


def function_23(x):
    return x[0]**2 + x[1]**2

ainit_x = np.array([-3.0, 4.0])
print(gradient_descent(function_23, init_x=ainit_x, lr=0.1, step_num=100))

import os
os.getcwd()
