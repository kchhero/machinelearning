import sys,os
import numpy as np
import mylibrary
import matplotlib.pylab as plt

class TwoLayerNet:    
    def __init__(self, input_size, hidden_size, output_size, weight_init_std=0.01):
        self.params = {}
        self.params['W1'] = weight_init_std * np.random.randn(input_size, hidden_size)
        self.params['b1'] = np.zeros(hidden_size)
        self.params['W2'] = weight_init_std * np.random.randn(hidden_size, output_size)
        self.params['b2'] = np.zeros(output_size)
        self.mylib = mylibrary.mylibs()
        
    def predict(self, x):
        W1, W2 = self.params['W1'], self.params['W2']
        b1, b2 = self.params['b1'], self.params['b2']

        a1 = np.dot(x, W1) + b1
#        print("a1,np.dot = "+str(a1))
        z1 = self.mylib.sigmoid(a1)
#        print("z1,sigmoid = "+str(z1))

        a2 = np.dot(z1, W2) + b2
#        print("a2,np.dot = "+str(a2))
        y = self.mylib.softmax(a2)
#        print("y,softmax = "+str(y))

        return y

    def loss(self, x, t):
        y = self.predict(x)

#        print("predict = "+str(y)+"\n")

        temp = self.mylib.cross_entropy_error(y,t)
#        print("cross_entropy_error = "+str(temp)+"\n")

        return temp

    def accuracy(self, x, t):
        y = self.predict(x)
        y = np.argmax(y, axis=1)
        t = np.argmax(t, axis=1)
        print("argmax y = "+str(y)+"\n")
        print("argmax t = "+str(t)+"\n")
        
        accuracy = np.sum(y == t) / float(x.shape[0])
        print("accuracy = "+str(accuracy))
        
        return accuracy

    def numerical_gradient(self, x, t):
        loss_W = lambda W: self.loss(x, t)

        grads = {}
        grads['W1'] = self.mylib.numerical_gradient(loss_W, self.params['W1'])
        grads['b1'] = self.mylib.numerical_gradient(loss_W, self.params['b1'])
        grads['W2'] = self.mylib.numerical_gradient(loss_W, self.params['W2'])
        grads['b2'] = self.mylib.numerical_gradient(loss_W, self.params['b2'])

        print("numerical_gradient = "+str(grads))
        
        return grads
