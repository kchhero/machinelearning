#!/usr/bin/env python
#-*- coding: utf-8 -*-
import IPython.nbformat.current as nbf
import sys

def gogo(s, d):
    nb = nbf.read(open(s, 'r'), 'py')
    nbf.write(nb, open(d, 'w'), 'ipynb')

def main(args):
    if len(args) < 2 :
        print "usage : need to python source and ipython source"
    else :
        gogo(args[0], args[1])
    
if __name__ == "__main__":
    args = sys.argv[1:]
    main(args)
    
