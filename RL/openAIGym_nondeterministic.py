import gym
import numpy as np
import matplotlib.pyplot as plt

env = gym.make('FrozenLake-v0')
env.render()

def playgame() :
    Q = np.zeros([env.observation_space.n, env.action_space.n])
    num_episodes = 2000
    alpha = .85
    dis = .99  # discount factor
   
    rList = []
    for i in range(num_episodes):
        state = env.reset()
        rAll = 0
        done = False

        while not done:
            action = np.argmax(Q[state, :] + np.random.randn(1, env.action_space.n) / (i+1))

            new_state, reward, done,_ = env.step(action)

            Q[state, action] = (1-alpha)*Q[state, action] + alpha*(reward + dis*np.max(Q[new_state,:])) # discount reward

            rAll += reward
            state = new_state

        #print(Q[state,:])
        rList.append(rAll)

    print("Success rate: %.3f"%(sum(rList)/num_episodes))
    print("Final Q-Table Values")
    print("LEFT DOWN RIGHT UP")
    print(Q)
    plt.bar(range(len(rList)), rList, color="blue")
    plt.show()

playgame()
