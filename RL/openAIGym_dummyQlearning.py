import gym
import numpy as np
import matplotlib.pyplot as plt
from gym.envs.registration import register
import random as pr

def rargmax(vector):
    """
    vector shape [0 0 0 0]
    """
    m = np.amax(vector) # vector에서 가장 큰 값을 얻어온다.
    indices = np.nonzero(vector == m)[0] # m과 일치하는 vector의 index, 즉 max index
    return pr.choice(indices) # m과 일치하는 vector의 index는 여러개가 있을 수 있으므로
                              # 그중에서 하나를 random하게 가져온다.

# gym package : 아래 소스 참고
#/usr/local/lib/python3.5/dist-packages/gym/envs/registration.py
register(
        id='FrozenLake-v3',
        entry_point='gym.envs.toy_text:FrozenLakeEnv',
        kwargs={'map_name' : '4x4', 'is_slippery': False}
        )

env = gym.make('FrozenLake-v3')
env.render()

def playgame() :
    """
    Q shape [[] [] [] []
             [] [] [] []
             [] [] [] []
             [] [] [] []]
    """
    Q = np.zeros([env.observation_space.n, env.action_space.n])
    num_episodes = 2000

    rList = []
    for i in range(num_episodes):
        state = env.reset()
        rAll = 0
        done = False
        #print("-------------------------")
        while not done:
            #print("state %d"%state)
            action = rargmax(Q[state, :])
            new_state, reward, done,_ = env.step(action)

            Q[state, action] = reward + np.max(Q[new_state,:])            
            print(np.max(Q[new_state,:]), "    reward %d"%reward)

            rAll += reward
            state = new_state

        print(Q[state,:])
        rList.append(rAll)

#    print("Success rate: %.3f"%(sum(rList)/num_episodes))
#    print("Final Q-Table Values")
    print("LEFT DOWN RIGHT UP")
    print(Q)
#    plt.bar(range(len(rList)), rList, color="blue")
#    plt.show()

playgame()
